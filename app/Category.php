<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";
    protected $guarded = [];


    public function products()
    {
        return $this->belongsTo('App\Product', 'category_id','id');
    }

    public function allProducts()
    {
        return $this->belongsToMany('App\Product','products','category_id');
    }
}
