<?php

namespace App\Http\Controllers;

use PHPHtmlParser\Dom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BotController extends Controller
{
    public function main(){
        $dj_urls = array();

        $dom = new Dom;
        $dom->loadFromUrl('http://thedjlist.com/djs/');
        $dj_list = $dom->find('#djrankcontainer');
        $data = $dj_list->innerHtml;


        $dom = new Dom;
        $dom->load($data);
        $a = $dom->find('a');
        for ($i = 0 ; $i < 10 ; $i++){
            array_push($dj_urls,$a[$i]->getAttribute('href'));
        }
        foreach ($dj_urls as $url){

            $dom = new Dom;
            $dom->loadFromUrl($url);
            $dj = $dom->find('h1');
            $dj_name = $dj->text;
            $dj = $dom->find('.number-tag-profile ');
            $dj_rank = substr( $dj->text,1);

            $dj = $dom->find('.photo-user-small');
            $style = $dj->getAttribute('style');
            preg_match("#https(.*?)jpg#",$style,$image);
            $dj_image = $image[0];

            $dom->loadFromUrl($url.'/info');
            $dj = $dom->find('#djcontent');
            $data = $dj->innerHtml;
            preg_match_all("@<p>(.*?)</p>@si",$data,$output);
            $bio = implode('<br />',$output[1]);

            $import_data = array(
                'name' => $dj_name,
                'rank' => $dj_rank,
                'image' => $dj_image,
                'bio' => $bio
                );
            DB::table('dj_list')->insert($import_data);

        }


    }
}
